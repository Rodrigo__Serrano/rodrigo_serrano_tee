import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  listaContas;

  constructor(
    private conta: ContaService,
    private alert: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    const url = this.router.url
    const tipo = url.split('/')[2]
    this.conta.lista(tipo).subscribe(x => this.listaContas = x);
  }

  async remove(conta){
    const confirm = await this.alert.create({
      header: 'Remover Conta',
      message: 'Deseja realmente deletar essa conta ?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Deletar',
        handler: () => this.conta.remove(conta)
      }
    ]
    });

    confirm.present();
  }

  async edita(conta){
    const confirm = await this.alert.create({
      header: 'Editar conta',
      inputs: [
        {
          name: 'parceiro',
          placeholder: 'Parceiro comercial',
          value: conta.parceiro
        },
        {
          name: 'descricao',
          placeholder: 'Descrição',
          value: conta.descricao
        },
        {
          name: 'valor',
          type: 'number',
          value: conta.valor
        }
      ],
      message: 'Deseja realmente deletar essa conta ?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Editar',
        handler: (data) => {
          conta.parceiro = data.parceiro
          conta.descricao = data.descricao
          conta.valor = data.valor
          this.conta.edita(conta)
        }
      }
    ]
    });

    confirm.present();
  }
}
