import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ContaService {
  colletion: AngularFirestoreCollection;

  constructor(
    private db: AngularFirestore,
  ) { }

  registraConta(conta){
    conta.id = this.db.createId();
    this.colletion = this.db.collection('conta');
    return this.colletion.doc(conta.id).set(conta)
  }

  lista(tipo){
    this.colletion = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.colletion.valueChanges();
  }

  listaAll(){
    this.colletion = this.db.collection('conta');
    return this.colletion.valueChanges();
  }

  remove(conta){
    this.colletion = this.db.collection('conta');
    this.colletion.doc(conta.id).delete();
  }

  // totais(conta){
  //     this.conta.listaAll().subscribe(data =>{
  //       this.valoresPagar = 0;
  //       this.valoresReceber = 0;
  //       this.saldo = 0;
  //       this.countPagar = 0;
  //       this.countReceber = 0;
  //       data.map(item =>{
  //         if(item.tipo === 'pagar'){
  //           this.valoresPagar += Number(item.valor);
  //           this.countPagar++;
  //         }else{
  //           this.valoresReceber += Number(item.valor);
  //           this.countReceber++;
  //         }
  //         this.saldo = (this.valoresReceber - this.valoresPagar)
  //         console.log(this.countPagar);
  //     });
  //   });
  // }

  totais(tipo){
    this.colletion = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.colletion.get();
  }

  edita(conta){
    this.colletion = this.db.collection('conta');
    this.colletion.doc(conta.id).update(conta);
  }
}
