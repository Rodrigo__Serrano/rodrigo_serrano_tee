import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {

  listaContas;
  tipo;
  constructor(
    private conta: ContaService,
    private alert: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    this.tipo = this.router.url.split('/')[2];
    this.conta.lista(this.tipo).subscribe(x => this.listaContas = x);
  }

  async remove(conta){
    const confirm = await this.alert.create({
      header: 'Remover Conta',
      message: 'Deseja realmente deletar essa conta ?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Deletar',
        handler: () => this.conta.remove(conta)
      }
    ]
    });

    confirm.present();
  }

  async edita(conta){
    const confirm = await this.alert.create({
      header: 'Editar conta',
      inputs: [
        {
          name: 'parceiro',
          placeholder: 'Parceiro comercial',
          value: conta.parceiro
        },
        {
          name: 'descricao',
          placeholder: 'Descrição',
          value: conta.descricao
        },
        {
          name: 'valor',
          type: 'number',
          value: conta.valor
        }
      ],
      message: 'Deseja realmente deletar essa conta ?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Editar',
        handler: (data) => {
          const obj = { ...conta, ...data };

          this.conta.edita(obj)
        }
      }
    ]
    });

    confirm.present();
  }
}
